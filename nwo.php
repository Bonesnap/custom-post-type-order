<?php

/**
 * Adds the New World Order menu item to the WordPress admin menu.
 *
 * @since 0.1.0
 */
function nwo_add_menu_page()
{
    add_menu_page('New World Order', 'New World Order', 'administrator', 'nwo', 'nwo_print_menu_page', 'dashicons-editor-ol');
}

/**
 * Prints the New World Order menu page.
 *
 * @since 0.1.0
 */
function nwo_print_menu_page()
{

}