<?php
/**
 * Plugin Name: New World Order
 * Description: Facilitates the ability to control the order of custom post types.
 * Author: Brent Tilley
 * Author URI: https://bitbucket.org/Bonesnap
 * Version: 0.1.0
 */

require_once('nwo.php');

if(!defined('NWO_PATH'))
{
    define('NWO_PATH', '/wp-content/nwo-order/');
}

/**
 * Registers the plugin.
 *
 * This function currently adds the following options to the WordPress options table:
 *
 * nwo_registered_datetime: The date and time of when the plugin was registered.
 *
 * @since 0.1.0
 */
function nwo_activation()
{
    global $wp_version;

    $php = '5.3';
    $wp = '4.2.2';

    // Make sure PHP is at least 5.3
    if(version_compare(PHP_VERSION, $php, '<'))
    {
        deactivate_plugins(basename(__FILE__));
        wp_die('<p>This plugin cannot be activated because it requires a PHP version greater than '.PHP_VERSION.' Your PHP version can be updated by your hosting company.</p><a href="'.admin_url('plugins.php').'">Go back</a>');
    }

    // Make sure WordPress is at least 4.2.2
    if(version_compare($wp_version, $wp, '<'))
    {
        deactivate_plugins(basename(__FILE__));
        wp_die('<p>This plugin cannot be activated because it requires a WordPress version of at least '.$wp.'. Please go to Dashboard &#9656; Updates to download the latest version of WordPress.');
    }

    update_option('nwo_registered_datetime', date('Y-m-d H:i:s'));
}
register_activation_hook(__FILE__, 'nwo_activation');

/**
 * This function removes the following options from the WordPress options table:
 *
 * nwo_registered_datetime
 *
 * @since 0.1.0
 */
function nwo_deactivation()
{
    delete_option('nwo_registered_datetime');
}
register_deactivation_hook(__FILE__, 'nwo_deactivation');

add_action('admin_menu', 'nwo_add_menu_page');